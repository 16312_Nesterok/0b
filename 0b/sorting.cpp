#include <list>
#include "sorting.h"

namespace Sorting {

	std::list<std::string> sort_strings(std::list<std::string> lst)
	{
		lst.sort();
		return lst;
	}
}