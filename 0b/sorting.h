#pragma once

#include <list>

namespace Sorting 
{
	std::list<std::string> sort_strings(std::list<std::string>);
}