#include <fstream>
#include <string>
#include <list>
#include <iostream>
#include "sorting.h"

using namespace std;

int main(int argc, char* argv[])
{
	ifstream in(argv[1]);
	ofstream out(argv[2]);
	list<string> text;
	string buf;

	cout << "Sorting started! Please wait :)" << endl;
	while (!in.eof())
	{
		getline(in, buf);
		text.push_back(buf);
	}
	text = Sorting::sort_strings(text);

	while (!text.empty())
	{
		out.write(text.front().c_str(), text.front().size());
		text.pop_front();
		if(!text.empty())
			out.put('\n');
	}

	cout << "Done!" << endl;
	return 0;
}